﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using SampleApp.Core.ByznyzLogic;
using SampleApp.Core.Commands;

namespace SampleApp.Core.CommandHandlers
{
	public class SomeCommandHandler : IRequestHandler<SomeCommand>
	{
		private readonly ILogger _logger;

		public SomeCommandHandler(ILogger logger)
		{
			_logger = logger;
		}

		public Task<Unit> Handle(SomeCommand request, CancellationToken cancellationToken)
		{
			_logger.LogInformation("Actual handler {Handler}... handling on thread {ThreadId}", GetType().Name, System.Environment.CurrentManagedThreadId);
			ImportantStuff.DoBusiness();
			return Unit.Task;
		}
	}
}