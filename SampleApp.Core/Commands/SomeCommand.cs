﻿using MediatR;
using SampleApp.Core.Pipeline.CommandInterfaces.Analytics;
using SampleApp.Core.Pipeline.CommandInterfaces.Thread;

namespace SampleApp.Core.Commands
{
	public class SomeCommand :
		IAnalyticsButtonClick, //will be handled by analytics handler
		IThreadedRequest, // will be run on a different thread
		IRequest<Unit> // mediatr marker interface - command with no response
	{
	}
}