﻿using System.Threading.Tasks;
using SampleApp.Core.Pipeline.CommandInterfaces.Analytics;

namespace SampleApp.Core.Analytics
{
	public interface IAnalyticsService
	{
		Task SendAnalytics(IAnalyticsAction analyticsAction);
	}
}