﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SampleApp.Core.Pipeline.CommandInterfaces.Analytics;

namespace SampleApp.Core.Analytics
{
	public class AnalyticsService : IAnalyticsService
	{
		private readonly ILogger _logger;

		public AnalyticsService(ILogger logger)
		{
			_logger = logger;
		}

		public async Task SendAnalytics(IAnalyticsAction analyticsAction)
		{
			// call one of few analytics methods - uiClicked, uiTabShown etc. based on the type/props
			_logger.LogInformation("Sending analytics on thread {threadId}", System.Environment.CurrentManagedThreadId);
		}
	}
}