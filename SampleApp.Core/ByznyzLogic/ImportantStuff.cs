﻿namespace SampleApp.Core.ByznyzLogic
{
	public static class ImportantStuff
	{
		public static void DoBusiness()
		{
			var number = int.MaxValue;
			var isPrime = true;
			for (var i = 2; i < number / 2; i++)
			{
				if (number % i == 0)
				{
					isPrime = false;
					break;
				}
			}
		}
	}
}