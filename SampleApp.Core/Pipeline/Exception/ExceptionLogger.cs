﻿using MediatR.Pipeline;
using Microsoft.Extensions.Logging;

namespace SampleApp.Core.Pipeline.Exception
{
	public class ExceptionLogger<TRequest, TException> : RequestExceptionAction<TRequest, TException> where TException : System.Exception
	{
		private readonly ILogger _logger;

		public ExceptionLogger(ILogger logger)
		{
			_logger = logger;
		}

		protected override void Execute(TRequest request, TException exception)
		{
			// won't be executed if exception is caught by any exception handler
			_logger.LogError(exception, "Shit happened but I logged it!");
		}
	}
}