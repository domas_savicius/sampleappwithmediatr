﻿using MediatR.Pipeline;
using Microsoft.Extensions.Logging;

namespace SampleApp.Core.Pipeline.Exception
{
	public class ExceptionHandler<TRequest, TResponse, TException> : RequestExceptionHandler<TRequest, TResponse, TException> where TException : System.Exception
	{
		private readonly ILogger _logger;

		public ExceptionHandler(ILogger logger)
		{
			_logger = logger;
		}

		protected override void Handle(TRequest request, TException exception, RequestExceptionHandlerState<TResponse> state)
		{
			_logger.LogError("Shit happened, im not handling it :/");
		}
	}
}