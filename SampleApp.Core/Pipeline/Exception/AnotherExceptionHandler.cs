﻿using MediatR.Pipeline;
using Microsoft.Extensions.Logging;

namespace SampleApp.Core.Pipeline.Exception
{
	public class AnotherExceptionHandler<TRequest, TResponse, TException> : RequestExceptionHandler<TRequest, TResponse, TException> where TException : System.Exception
	{
		private readonly ILogger _logger;

		public AnotherExceptionHandler(ILogger logger)
		{
			_logger = logger;
		}

		protected override void Handle(TRequest request, TException exception, RequestExceptionHandlerState<TResponse> state)
		{
			_logger.LogError("Shit happened but I handled it");
			// if it's marked as handled it will not be passed down to any other exception requst handler or actionahndler 
			state.SetHandled(default); // we handle it; exception wont be thrown to the class that executed this command
		}
	}
}