﻿namespace SampleApp.Core.Pipeline.CommandInterfaces.Analytics
{
	public interface IAnalyticsButtonClick : IAnalyticsAction
	{
		//Properties like button name, form name etc. 
	}
}