﻿namespace SampleApp.Core.Pipeline.CommandInterfaces.Thread
{
	public interface IThreadedRequest
	{
		// we can have an enum or something
		// specifying which dedicated thread to use when handling this command 
	}
}