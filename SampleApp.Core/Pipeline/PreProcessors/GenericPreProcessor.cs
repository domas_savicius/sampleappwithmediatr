﻿using System.Threading;
using System.Threading.Tasks;
using MediatR.Pipeline;
using Microsoft.Extensions.Logging;

namespace SampleApp.Core.Pipeline.PreProcessors
{
	public class GenericPreProcessor<TRequest> : IRequestPreProcessor<TRequest>
	{
		private readonly ILogger _logger;

		public GenericPreProcessor(ILogger logger)
		{
			_logger = logger;
		}


		public Task Process(TRequest request, CancellationToken cancellationToken)
		{
			_logger.LogInformation("==============================================================================================");
			_logger.LogInformation("Executed before handling");
			return Task.CompletedTask;
		}
	}
}