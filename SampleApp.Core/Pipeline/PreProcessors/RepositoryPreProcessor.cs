﻿using System.Threading;
using System.Threading.Tasks;
using MediatR.Pipeline;
using Microsoft.Extensions.Logging;

namespace SampleApp.Core.Pipeline.PreProcessors
{
	public class RepositoryPreProcessor<TRequest> : IRequestPreProcessor<TRequest>
	{
		private readonly ILogger _logger;

		public RepositoryPreProcessor(ILogger logger)
		{
			_logger = logger;
		}

		public Task Process(TRequest request, CancellationToken cancellationToken)
		{
			_logger.LogInformation("Saving request to event store");
			return Task.CompletedTask;
		}
	}
}