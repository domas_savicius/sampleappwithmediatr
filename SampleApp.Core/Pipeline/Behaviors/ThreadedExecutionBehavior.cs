﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using SampleApp.Core.Pipeline.CommandInterfaces.Thread;

namespace SampleApp.Core.Pipeline.Behaviors
{
	public class ThreadedExecutionBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IThreadedRequest
	{
		private readonly ILogger _logger;

		public ThreadedExecutionBehavior(ILogger logger)
		{
			_logger = logger;
		}

		public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
		{
			// run on a dedicated thread that's specified in IThreadedRequest
			// if the request (command) is not IThreadedRequest this will not be called
			_logger.LogInformation(@"Executing in a dedicated thread \o/");
			return await Task.Run(() => next(), cancellationToken);
		}
	}
}