﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;

namespace SampleApp.Core.Pipeline.Behaviors
{
	public class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
	{
		private readonly ILogger _logger;

		public LoggingBehavior(ILogger logger)
		{
			_logger = logger;
		}

		public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
		{
			var cmdType = request?.GetType().Name;

			var threadId = System.Environment.CurrentManagedThreadId;

			_logger.LogInformation("Executing command {Cmd}, on thread: {Id}", cmdType, threadId);

			var stopwatch = Stopwatch.StartNew();
			try
			{
				var result = await next().ConfigureAwait(false);
				return result;
			}
			// we can have catch here too, to handle every exception
			// or we can have exceptionrequest/action handlers
			// for all or some specific exceptions
			finally
			{
				stopwatch.Stop();
				_logger.LogInformation("Finished execution of command {cmd}, on thread: {id}. Took: {time} ms", cmdType, threadId, stopwatch.ElapsedMilliseconds);
			}
		}
	}
}