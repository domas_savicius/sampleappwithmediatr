﻿using System.Threading;
using System.Threading.Tasks;
using MediatR.Pipeline;
using SampleApp.Core.Analytics;
using SampleApp.Core.Pipeline.CommandInterfaces.Analytics;

namespace SampleApp.Core.Pipeline.PostProcessors
{
	public class AnalyticsPostProcessor<TRequest, TResponse> : IRequestPostProcessor<TRequest, TResponse> where TRequest : IAnalyticsAction
	{
		private readonly IAnalyticsService _analyticsService;

		public AnalyticsPostProcessor(IAnalyticsService analyticsService)
		{
			_analyticsService = analyticsService;
		}

		public async Task Process(TRequest request, TResponse response, CancellationToken cancellationToken)
		{
			// pass this down to a class that handles analytics
			// In this case it is done AFTER the command is handled, but we can do it before and/or after too
			// we can execute this on our dedicated analytics thread too
			_ = Task.Run(() => _analyticsService.SendAnalytics(request), cancellationToken);
		}
	}
}