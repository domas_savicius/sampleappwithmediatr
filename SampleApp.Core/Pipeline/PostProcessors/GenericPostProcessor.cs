﻿using System.Threading;
using System.Threading.Tasks;
using MediatR.Pipeline;
using Microsoft.Extensions.Logging;

namespace SampleApp.Core.Pipeline.PostProcessors
{
	public class GenericPostProcessor<TRequest, TResponse> : IRequestPostProcessor<TRequest, TResponse>
	{
		private readonly ILogger _logger;

		public GenericPostProcessor(ILogger logger)
		{
			_logger = logger;
		}

		public Task Process(TRequest request, TResponse response, CancellationToken cancellationToken)
		{
			_logger.LogInformation("Executed after handling");
			_logger.LogInformation("==============================================================================================");
			return Task.CompletedTask;
		}
	}
}