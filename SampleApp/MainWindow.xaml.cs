﻿using System.Windows;
using System.Windows.Controls;
using MediatR;
using SampleApp.Core.Commands;

namespace SampleApp
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private readonly IMediator _mediator;

		public MainWindow(IMediator mediator)
		{
			_mediator = mediator;
			InitializeComponent();
		}

		private async void ButtonBase_OnClick(object sender, RoutedEventArgs e)
		{
			(sender as Button).IsEnabled = false;

			var command = new SomeCommand();
			await _mediator.Send(command);

			(sender as Button).IsEnabled = true;
		}

		private async void ButtonBase_OnClick2(object sender, RoutedEventArgs e)
		{
			(sender as Button).IsEnabled = false;

			var command = new SomeOtherCommand();
			await _mediator.Send(command);

			(sender as Button).IsEnabled = true;
		}
	}
}