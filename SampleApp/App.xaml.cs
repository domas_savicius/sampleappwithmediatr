﻿using System.Windows;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SampleApp.Core.Analytics;
using SampleApp.Core.Commands;
using SampleApp.Core.Pipeline.Behaviors;

namespace SampleApp
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private readonly IHost _host;

		public App()
		{
			_host = new HostBuilder()
				.ConfigureServices(ConfigureServices)
				// .ConfigureLogging(builder => builder.AddConsole().AddSimpleConsole().SetMinimumLevel(LogLevel.Trace))?
				.Build();

			using var serviceScope = _host.Services.CreateScope();
			var main = serviceScope.ServiceProvider.GetRequiredService<MainWindow>();
			main.Show();
		}

		private static void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<MainWindow>();
			services.AddLogging(builder =>
			{
				builder.SetMinimumLevel(LogLevel.Trace);
				builder.AddDebug();
				builder.AddConsole();
			});
			services.AddSingleton<ILogger>(s => s.GetRequiredService<ILogger<App>>());
			AddMediatr(services);
			AddAppServices(services);
		}

		private static void AddAppServices(IServiceCollection services)
		{
			services.AddTransient<IAnalyticsService, AnalyticsService>();
		}

		private static void AddMediatr(IServiceCollection services)
		{
			// will be executed in order of registration
			services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));

			// this registers the whole mediatr pipeline
			// in this case our LoggingBehavior will be the first one
			services.AddMediatR(typeof(SomeCommand).Assembly);

			services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ThreadedExecutionBehavior<,>));

			// These are registered automatically, so sadly we can't enforce order like with pipeline behaviors :/

			// pre processor(s)
			// services.AddTransient(typeof(IRequestPreProcessor<>), typeof(GenericPreProcessor<>));
			// services.AddTransient(typeof(IRequestPreProcessor<>), typeof(RepositoryPreProcessor<>));

			// post processor(s)
			// services.AddTransient(typeof(IRequestPostProcessor<,>), typeof(GenericPostProcessor<,>));
			// services.AddTransient(typeof(IRequestPostProcessor<,>), typeof(AnalyticsPostProcessor<,>));

			// Exception handler(s)
			// services.AddTransient(typeof(AsyncRequestExceptionHandler<,>), typeof(ExceptionHandlingBehavior<,>));
		}
	}
}