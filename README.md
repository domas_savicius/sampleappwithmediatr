# Example of architecture with MediatR

* ViewModels (MainWindow.xaml.cs 😁) only interact with core services via commands using MediatR
* On button click:
	* SomeCommand is sent to the pipeline (in order):
		* LoggingBehavior - logs before executing the next handler
		* Every IRequestPreProcessor<,> is executed - not in order, although there is some ordering based on namespaces, names etc.
			* RepositoryPreProcessor - saves the even to event store
			* GenericPreProcessor - just for example
			* Others that may be registered...
		* Every IPipelineBehavior<,> is executed in order of registration
			* ThreadedExecutionBehavior - is executed if command implements IThreadedRequest. Command handler is executed on a different thread
			* Others that may be registered...
		* **SomeCommandHandler** - the actual command handler
		* Every IRequestPostProcessor<,> is executed - same as IRequestPreProcessor<,>
			* AnalyticsPostProcessor - is executed if command implements IAnalyticsAction.
			* GenericPostProcessor - just for example
			* Others that may be registered...
		*  LoggingBehavior - logs after executing the handler
